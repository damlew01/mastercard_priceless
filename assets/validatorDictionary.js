const dictionary = {
  pl: {
    messages: {
      required: `To pole jest wymagane.`,
      min: (field, params) =>
        `To pole powinno zawierać conajmniej ${params[0]}&nbsp;znaki.`,
      max: (field, params) =>
        `To pole powinno zawierać maksymalnie ${params[0]}&nbsp;znaki.`,
      length: (field, params) =>
        `To pole powinno zawierać ${params[0]}&nbsp;znaków.`,
      numeric: `To pole powinno zawierać tylko cyfry`,
      regex: `To pole jest nieprawidłowe`
    }
  }
}

export default dictionary
