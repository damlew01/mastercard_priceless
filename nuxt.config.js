import pkg from './package'

export default {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      {
        src: `https://polyfill.io/v3/polyfill.min.js?features=WeakSet%2CObject.entries%2HTMLPictureElement`,
        body: true
      }
    ]
  },
  server: {
    host: '0.0.0.0'
  },
  /*
   ** Customize the progress-bar color
   */
  loading: true,
  scrollToTop: true,
  /*
   ** Global CSS
   */
  css: ['normalize.css', '@/assets/styles/main.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '@/plugins/index.js',
    {
      src: '@/plugins/aos.js',
      ssr: false
    },
    {
      src: '@/plugins/nossr.js',
      ssr: false
    }
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/style-resources'
  ],
  styleResources: {
    scss: ['@/assets/styles/mixin.scss']
  },
  /*
   ** Axios module configuration
   */
  axios: {
    browserBaseURL:
      process.env.NODE_ENV === 'production' ? '' : 'http://localhost:8000/api'
  },
  router: {
    base: '/mastercard_priceless/'
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    analyze: true,
    transpile: [/^vue2-google-maps($|\/)/, 'vue-particles'],
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      config.resolve.alias.vue = 'vue/dist/vue.common'

      // Handle Svg
      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))
      svgRule.test = /\.(png|jpe?g|gif|webp)$/
      config.module.rules.push({
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          },
          {
            loader: 'vue-svg-loader'
          }
        ]
      })
    }
  }
}
