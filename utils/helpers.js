import { debounce } from 'lodash'

export function outerHeight(el) {
  let height = el.offsetHeight
  const style = getComputedStyle(el)

  height += parseInt(style.marginTop) + parseInt(style.marginBottom)
  return height
}

export function debouncedResize(fn) {
  window.addEventListener('resize', debounce(fn, 400))
}
