import Form from './Form'
import StepTemplate from './StepTemplate'

export const Step1 = {
  components: {
    StepTemplate,
    Form
  },
  template: `<StepTemplate>
    <template slot="heading">1. Zarejestruj się</template>
    <template slot="subtitle">
    Uzupełnij dane i płać kartą Mastercard gdzie chcesz.
    </template>
    <Form />
  </StepTemplate>`
}

export const Step2 = {
  components: {
    StepTemplate
  },
  template: `<StepTemplate>
      <template slot="heading">2. Dokonaj transakcji</template>
      <template slot="subtitle">Wykonaj <b>1 transakcję</b> w dowolnym sklepie stacjonarnym
      lub internetowym za minimum <b>40 zł</b> w terminie od <b>06 do 19.06.19</b>
       </template>
    </StepTemplate>`
}

export const Step3 = {
  components: {
    StepTemplate
  },
  template: `<StepTemplate>
      <template slot="heading">3. Otrzymaj voucher</template>
      <template slot="subtitle">W ciągu <b>14 dni</b> od realizacji transakcji otrzymasz e-mailem nagrodę
      w postaci <b>vouchera do sieci sklepów Euro RTV AGD o wartości 20 zł.</b>
       
      </template>
      <img style="max-width:70%;" src="${require('assets/images/euro20-voucher-card.png')}"  
      srcset="${require('assets/images/euro20-voucher-card@2x.png')} 2x,
      ${require('assets/images/euro20-voucher-card@3x.png')} 3x"/>
    </StepTemplate>`
}

export const Step4 = {
  components: {
    StepTemplate
  },
  template: `<StepTemplate>
        <template slot="heading">4. Zyskuj podwójnie</template>
        <template slot="subtitle">
        <ol>
          <li><p> Wykonaj <b>2 dodatkowe transakcje</b> w terminie <b>od 11.07 do 22.07</b></p></li>
          <li><p>W ciągu <b>14 dni</b> od realizacji transakcji otrzymasz e-mailem nagrodę
          w postaci <b>vouchera do sieci stacji paliw Circle K o wartości 20 zł.</b></p></li>
        </ol> 
        </template> 
        <img style="max-width:70%;" src="${require('assets/images/ciclek20-voucher-card.png')}" 
        srcset="${require('assets/images/ciclek20-voucher-card@2x.png')} 2x,
        ${require('assets/images/ciclek20-voucher-card@3x.png')} 3x"/>
      </StepTemplate>`
}
