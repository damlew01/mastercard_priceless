export const state = () => ({
  isMobile: false
})

export const mutations = {
  setMobile(state, value) {
    state.isMobile = value
  }
}
