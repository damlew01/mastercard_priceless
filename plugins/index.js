import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueScrollTo from 'vue-scrollto'
import VueTheMask from 'vue-the-mask'
import VTooltip from 'v-tooltip'

import validatorDict from '~/assets/validatorDictionary'

Vue.use(VeeValidate, {
  locale: 'pl',
  dictionary: validatorDict
})

Vue.use(VueScrollTo, {
  container: 'body'
})

Vue.use(VueTheMask)
Vue.use(VTooltip)
