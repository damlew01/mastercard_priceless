import AOS from 'aos'
import 'aos/dist/aos.css'

// eslint-disable-next-line new-cap
AOS.init({
  offset: 0,
  delay: 300,
  throttleDelay: 50,
  disable: 'mobile'
})
